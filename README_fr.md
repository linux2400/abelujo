Abelujo - logiciel libre de gestion de petite librairie indépendante (et disquaires)
====================================================================

Projet actuellement en chantier. Il est néanmoins possible de:

-   rechercher des **livres** par mots-clefs ou par n°ISBN/EAN. La recherche
    s'effectue sur le site [chapitre.com](https://www.chapitre.com),
-   rechercher des **CDs** (via [discogs.com](http://www.discogs.com),
-   choisir combien d'exemplaires sont à ajouter dans le stock, les
    bouger de lieu,
-   chercher un titre dans son stock et le marquer comme vendu (ce qui
    décrémente de 1 son nombre d'exemplaires).
-   **importer des données** depuis un fichier **LibreOffice Calc** (.ods). Voir la [documentation utilisateur](doc/french/index.rst "doc utilisateur").


Nous nous basons sur les spécifications fonctionnelles du projet Ruche,
auxquelles nous avons participé:
<http://ruche.eu.org/wiki/Specifications_fonctionnelles>. Nous avons
écrit ce que nous avons compris de la gestion des dépôts, des commandes,
etc. N'hésitez donc pas à les lire et à nous dire si ce que nous
commençons correspondra à vos besoins.

**Abelujo** signifie Ruche en Espéranto.

Installation et développement
=============================

Nous vous laissons le soin de [lire les instructions en anglais](README.md "").

Ci-dessous une capture d'écran de la recherche dans la base de données:

![chercher une notice](doc/abelujo-collection.png)
